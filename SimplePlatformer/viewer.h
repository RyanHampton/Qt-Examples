#ifndef VIEWER_H
#define VIEWER_H

#include <QObject>
#include <QQuickItem>
#include <memory>
#include <QQuickPaintedItem>
#include <QPainterPath>
#include <QString>

#include "hexagonalrenderer.h"
#include "isometricrenderer.h"
#include "map.h"
#include "mapobject.h"
#include "mapreader.h"
#include "objectgroup.h"
#include "orthogonalrenderer.h"
#include "staggeredrenderer.h"
#include "tilelayer.h"
#include "tileset.h"

namespace Tiled {
    class Map;
    class MapRenderer;
    class Layer;
}

class Viewer : public QQuickPaintedItem{
    Q_OBJECT
    Q_PROPERTY(bool rightAligned READ isRightAligned WRITE setRightAligned NOTIFY rightAlignedChanged)

  public:
    explicit Viewer(QQuickItem *parent = nullptr);
    void paint(QPainter *painter);

    bool isRightAligned();
    void setRightAligned(bool rightAligned);

  signals:
    void rightAlignedChanged();

  private:
    bool renderTiledMap();
    bool shouldDrawLayer(const Tiled::Layer *layer);

    bool mRightAligned;
    std::unique_ptr<Tiled::Map> mMap;
    std::unique_ptr<Tiled::MapRenderer> mRenderer;
    const QString mMapFileName;
};


#endif // VIEWER_H
