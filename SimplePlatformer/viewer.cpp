#include "viewer.h"

using namespace Tiled;

Viewer::Viewer(QQuickItem *parent)
    : QQuickPaintedItem(parent), mRightAligned(false),
      mMapFileName("/home/rhampton/Projects/Qt-Examples/SimplePlatformer/Resources/SimplePlatformMap.tmx")
    {}


void Viewer::paint(QPainter *painter){

        bool success = renderTiledMap();

    QRect mapBoundingRect = mRenderer->mapBoundingRect();
    QSize mapSize = mapBoundingRect.size();
    QPoint mapOffset = mapBoundingRect.topLeft();
    qreal xScale = 0.5, yScale = 0.5;

//    if (mSize > 0) {
//        xScale = (qreal) mSize / mapSize.width();
//        yScale = (qreal) mSize / mapSize.height();
//        xScale = yScale = qMin(1.0, qMin(xScale, yScale));
//    } else if (mTileSize > 0) {
//        xScale = (qreal) mTileSize / map->tileWidth();
//        yScale = (qreal) mTileSize / map->tileHeight();
//    } else {
//        xScale = yScale = mScale;
//    }

    QMargins margins = mMap->computeLayerOffsetMargins();
    mapSize.setWidth(mapSize.width() + margins.left() + margins.right());
    mapSize.setHeight(mapSize.height() + margins.top() + margins.bottom());

    mapSize.rwidth() *= xScale;
    mapSize.rheight() *= yScale;

    QImage image(mapSize, QImage::Format_ARGB32);
    image.fill(Qt::transparent);

    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setRenderHint(QPainter::SmoothPixmapTransform, true);
    painter->setTransform(QTransform::fromScale(xScale, yScale));

    painter->translate(margins.left(), margins.top());
    painter->translate(-mapOffset);

    // Perform a similar rendering than found in exportasimagedialog.cpp
    LayerIterator iterator(mMap.get());
    while (const Layer *layer = iterator.next()) {
        if (!shouldDrawLayer(layer))
            continue;

        const auto offset = layer->totalOffset();

        painter->setOpacity(layer->effectiveOpacity());
        painter->translate(offset);

        const TileLayer *tileLayer = dynamic_cast<const TileLayer*>(layer);
        //const ImageLayer *imageLayer = dynamic_cast<const ImageLayer*>(layer);

        if (tileLayer) {
            mRenderer->drawTileLayer(painter, tileLayer);
        }
//        } else if (imageLayer) {
//            renderer->drawImageLayer(&painter, imageLayer);
//        }

        painter->translate(-offset);
            update();
    }


    mMap.reset();
}


bool Viewer::shouldDrawLayer(const Layer *layer)
{
    if (layer->isObjectGroup() || layer->isGroupLayer())
        return false;

    return !layer->isHidden();
}

bool Viewer::isRightAligned()
{
    return mRightAligned;
}

void Viewer::setRightAligned(bool rightAligned)
{
    mRightAligned = rightAligned;
}

bool Viewer::renderTiledMap(){
    mRenderer.reset();

    MapReader reader;
    mMap.reset(reader.readMap(mMapFileName));
    if (!mMap) {
        qWarning("Error while reading \"%s\":\n%s",
                 qUtf8Printable(mMapFileName),
                 qUtf8Printable(reader.errorString()));
        return false;
    }

    switch (mMap->orientation()) {
    case Map::Isometric:
        mRenderer.reset(new IsometricRenderer(mMap.get()));
        break;
    case Map::Staggered:
        mRenderer.reset(new StaggeredRenderer(mMap.get()));
        break;
    case Map::Hexagonal:
        mRenderer.reset(new HexagonalRenderer(mMap.get()));
        break;
    case Map::Orthogonal:
    default:
        mRenderer.reset(new OrthogonalRenderer(mMap.get()));
        break;
    }

    return true;
}
